import numpy as np
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import axes3d
import time
inicio = time.time()

#Obtener Datos de constante gravitacional en unidades astronomicas, masa del sol y tiempo en años 

ConstanteGranc=6.67191*pow(10,-11)
masaDelSol=1.989*pow(10,30)
distanciaASOL=(1.495978707*pow(10,11))
Conversionnc=ConstanteGranc*pow((1/distanciaASOL),3)*(masaDelSol)*pow((31536000),2)

print('Constante Gravitacional en Unidades Astronomicas ',Conversionnc)


#Inicializar Datos

numeroParticulas=3
ciclos=1.19*pow(10,5)
factor=int(1.2*pow(10,3))

constGravitacional=Conversionnc
deltat=1 *pow(10,-4)

#Masa de la tierra

MasaTierra=(5.9722*pow(10,24))/(masaDelSol)

print('Masa Tierra ',MasaTierra)

masaJupiter=1.898*pow(10,27)/(masaDelSol)

print('Masa Jupiter ',masaJupiter)


#Velocidad inicial

VelocidadInicial=pow(ConstanteGranc*masaDelSol/distanciaASOL,1/2)*pow((1/(1000)),1)

print('Velocidad Inicial Km/s',VelocidadInicial)

VelocidadInicial=pow(constGravitacional,1/2)

print('Velocidad Inicial Unidades Astronomicas',VelocidadInicial)

velocidadJupiter=pow(constGravitacional/5.2,1/2)

print('Velocidad Inicial Jupiter',velocidadJupiter)
#Tiempo

print('Tiempo en dias terricolas ' ,float(ciclos*deltat*365))

matrizDatos = np.zeros((numeroParticulas, (int(ciclos)*3)+7))
matrizVelocidades = np.zeros((numeroParticulas,3))



def obtenerDatosParticula(matrix,x):

    return matrix[x]



def iniciarDatosUnaParticula(masa,x,y,z,Vx,Vy,Vz,numparticula,matrix):

    matrix[numparticula,0]=masa
    matrix[numparticula,1]=Vx
    matrix[numparticula,2]=Vy
    matrix[numparticula,3]=Vz
    matrix[numparticula,4]=x
    matrix[numparticula,5]=y
    matrix[numparticula,6]=z
    matrizVelocidades[numparticula,0]=Vx
    matrizVelocidades[numparticula,1]=Vy
    matrizVelocidades[numparticula,2]=Vz




# inicializar particulas sistema solar
iniciarDatosUnaParticula(MasaTierra,1,0,0,0,pow(constGravitacional,1/2),0,0,matrizDatos)
iniciarDatosUnaParticula(1,0,0,0,0,0,0,1,matrizDatos)
#iniciarDatosUnaParticula(MasaLuna,1+distanciatierraLuna,0,0,0,pow(constGravitacional,1/2)+velocidadluna,0,2,matrizDatos)

iniciarDatosUnaParticula(masaJupiter,5.2,0,0,0,velocidadJupiter,0,2,matrizDatos)

def obtenerPosicionParticula(matrix,numparticula,ciclo):
    arrayPosicion = np.zeros((3))
    arrayPosicion[0]=matrix[numparticula,4+(ciclo*3)]
    arrayPosicion[1]=matrix[numparticula,5+(ciclo*3)]
    arrayPosicion[2]=matrix[numparticula,6+(ciclo*3)]
    return arrayPosicion

def calcularPosicionParticula(matrix,numparticula,ciclo):
	tiempo=(deltat)

	arrayPosicionXAnterior=matrix[numparticula,4+(ciclo*3)]
	arrayPosicionYAnterior=matrix[numparticula,5+(ciclo*3)]
	arrayPosicionZAnterior=matrix[numparticula,6+(ciclo*3)]

	axCalculada=0.0
	ayCalculada=0.0
	azCalculada=0.0
	xCalculada=0.0
	yCalculada=0.0
	zCalculada=0.0
	norma=0.0
	
    
	for x in range(numeroParticulas):
     
		if numparticula!= x :
            #norma
	
			norma=pow(pow(matrix[x,4+(ciclo*3)]-matrix[numparticula,4+(ciclo*3)],2)+pow(matrix[x,5+(ciclo*3)]-matrix[numparticula,5+(ciclo*3)],2)+pow(matrix[x,6+(ciclo*3)]-matrix[numparticula,6+(ciclo*3)],2),3/2)
			#print(norma)
			axCalculada=axCalculada+((matrix[x,0]*(matrix[x,4+(ciclo*3)]-matrix[numparticula,4+(ciclo*3)]))/norma)
			ayCalculada=ayCalculada+((matrix[x,0]*(matrix[x,5+(ciclo*3)]-matrix[numparticula,5+(ciclo*3)]))/norma)
			azCalculada=azCalculada+((matrix[x,0]*(matrix[x,6+(ciclo*3)]-matrix[numparticula,6+(ciclo*3)]))/norma)
		
		

	axCalculada=axCalculada*constGravitacional
	ayCalculada=ayCalculada*constGravitacional
	azCalculada=azCalculada*constGravitacional

	if numparticula==0 and ciclo==1:
		global AceleracionInicialTierra
		AceleracionInicialTierra=pow(pow(axCalculada,2)+pow(ayCalculada,2)+pow(azCalculada,2),1/2)
		print('aceleracion tierra inicial',AceleracionInicialTierra)

	if numparticula==0 and ciclo==ciclos-1:
		global AceleracionFinalTierra
		AceleracionFinalTierra=pow(pow(axCalculada,2)+pow(ayCalculada,2)+pow(azCalculada,2),1/2)
		print('aceleracion tierra final',AceleracionFinalTierra)
		

	xCalculada=arrayPosicionXAnterior+(matrizVelocidades[numparticula,0]*tiempo)+((axCalculada*tiempo*tiempo)/2)
	yCalculada=arrayPosicionYAnterior+(matrizVelocidades[numparticula,1]*tiempo)+((ayCalculada*tiempo*tiempo)/2)
	zCalculada=arrayPosicionZAnterior+(matrizVelocidades[numparticula,2]*tiempo)+((azCalculada*tiempo*tiempo)/2)
    

	matrizVelocidades[numparticula,0]=matrizVelocidades[numparticula,0]+(axCalculada*tiempo)
	matrizVelocidades[numparticula,1]=matrizVelocidades[numparticula,1]+(ayCalculada*tiempo)
	matrizVelocidades[numparticula,2]=matrizVelocidades[numparticula,2]+(azCalculada*tiempo)

	if numparticula==0 and ciclo==ciclos-1:
			VelocidadFinalTierra=pow(pow(matrizVelocidades[numparticula,0],2)+pow(matrizVelocidades[numparticula,1],2)+pow(matrizVelocidades[numparticula,2],2),1/2)
			print('velocidad tierra final',VelocidadFinalTierra)

	matrix[numparticula,4+(ciclo*3)+3]=xCalculada
	matrix[numparticula,5+(ciclo*3)+3]=yCalculada
	matrix[numparticula,6+(ciclo*3)+3]=zCalculada
    

	return 1



#Calcular Posiciones
puerta =True
for v in range(int(ciclos)):
	
		
	for a in range(numeroParticulas):
		if puerta:

			if calcularPosicionParticula(matrizDatos,a,v)==0 :
				print('Choque de masas')
				puerta=False
			


#datos despues de simulación 

fin = time.time()
print('Tiempo maquina en segundos transcurrido de simulación ',fin-inicio)

#Graficar puntos
# Creamos la figura
fig = plt.figure()


# Agregamos un plano 3D
ax1 = fig.add_subplot(111, projection='3d')
ax1.set_xlabel('eje x')
ax1.set_ylabel('eje y')
ax1.set_zlabel('eje z')
plt.suptitle("Aproximación a orbita circular terrestre (Euler)")
ax1.set_title('Sol=rojo Tierra=Verde Jupiter=amarillo')

InformacionGrafica='Años terricolas '+str(ciclos*deltat) 
ax1.text(0.1, 0.5,0.85, InformacionGrafica, horizontalalignment='center', verticalalignment='center', transform=ax1.transAxes)

InformacionGrafica2='delta t (minutos) '+str(deltat*365*24*60)[0:6] 
ax1.text(0.1, 0.5,0.8, InformacionGrafica2, horizontalalignment='center', verticalalignment='center', transform=ax1.transAxes)

#if(puerta):
	#InformacionGrafica3='diferencia aceleracion(UA) '+str(AceleracionFinalTierra-AceleracionInicialTierra)[0:6] 
	#ax1.text(0.1, 0.5,0.75, InformacionGrafica3, horizontalalignment='center', verticalalignment='center', transform=ax1.transAxes)

InformacionGrafica3='Tiempo simulación (seg)'+str(fin-inicio)[0:6] 
ax1.text(0.1, 0.5,0.75, InformacionGrafica3, horizontalalignment='center', verticalalignment='center', transform=ax1.transAxes)

for v in range(int(ciclos/factor)):

	
	Posicion=obtenerPosicionParticula(matrizDatos,0,v*factor)
	ax1.scatter(Posicion[0], Posicion[1], Posicion[2], c='g', marker='o')

	Posicion=obtenerPosicionParticula(matrizDatos,2,v*factor)
	ax1.scatter(Posicion[0], Posicion[1], Posicion[2], c='y', marker='o')
	Posicion=obtenerPosicionParticula(matrizDatos,1,0)
	ax1.scatter(Posicion[0], Posicion[1], Posicion[2], c='r', marker='o')


	plt.pause(0.000000001)

plt.savefig('SistemaSolar.png')
plt.show()